# Pair Programming Techniques

## Driver - Navigator
### Background
This is _the_ classic pairing technique that most people are familiar with.
To understand what roles the _driver_ and _navigator_ take, it's useful to compare it to a road trip:

* The _driver_ takes instructions on where to go, and what to do. 
* The _navigator_ is freed up to think about the direction to take, and to plan the route, and think about pitfalls.


### How to do it
In a pair programming context, the _driver_ is the one doing the typing, the _navigator_ is guiding the driver in what code to write and where.
It's important to point out that the _driver_ shouldn't be passive and should be encouraged to:

* Ask questions about the suggested implementation / clarify instructions
* Push-back on directions if it will stop the pair being able to complete the code being written*
* Ask to switch roles when it's right to do so

\*The _driver_ should be careful to not switch to a _navigator_ role, just because the directions aren't the way _they_ would tackle the problem.

### When to switch roles

There are multiple options:

* Time-based - this could be based on Pomodoro, or shorter, or longer depending on the pair's preference
* Task-based - when a task (e.g. write a new function/test) is complete
* Idea-based - when it's clear the _driver_ has a direction that's different to the _navigator_ and both agree to swap

### Fun variations

#### Chess Clock
An interesting variation on the time-based switching is using chess clocks:

Each member of the pair starts with an equal amount of time on their clock (e.g. 30 mins). 
The person that is _navigator_ should have their clock running. When the roles switch, the _navigator_ hits their clock, which stops it from counting down, and starts the other pair member's clock. 

You can only be the _navigator_ if you have time left on your clock, and your clock is counting down - this prevents one person from hogging the position.

#### Hard Mode
The _driver_ does explicitly what the _navigator_ tells them to do, but nothing else. The only thing they can input is making the code syntax valid.

This allows less experienced developers to solve problems, without having to worry about how to [PHP|SQL|HTML|JavaScript], but they get no assistance so they really have to think through things themselves.

# Ping - Pong
## Background
Just like the game of ping-pong, this method has a lot of back and forth, and encourages a good TDD flow.

## How to do it
* Form a pair, A + B
* A starts by writing a test which doesn't pass
* Control passes to B
* B writes just enough code to make A's test pass
* B then writes their own test which doesn't pass
* Control passes to A
* A writes just enough code to make B's test pass
* A then writes another test...

This pattern continues until A and B agree that no more tests are necessary,
then the refactoring can begin:

* A refactors code
* If any tests break, A fixes them
* Control passes to B
* B refactors code
* If any tests break, B fixes them
* Control passes to A...

### Fun variations

#### Evil-mode (or Wiseguy Driven Development)
When writing the minimal amount of code to make a test pass, be deliberately adversarial e.g.
given a test such as:

```PHP
$actual = addNumbers(1, 2);
$expected = 3;
assertEqual($actual, $expected);
```

Write the function:
```PHP
public function addNumbers(int $num1, int $num2): int {
  return 3;
} 
```

This avoids tests that _pass by coincidence_ when code is changed, and results in a robust test suite.
May result in more tests than is strictly necessary.

## Promiscuous Pairing
### Background
Coined in a paper by Arlo Banshee, it's a bit like speed-dating, where pairs switch very frequently 
(it doesn't mandate dating co-workers, don't worry).

### How to do it
* Take at least four developers, A, B, C and D
* A pairs with B on task 1
* C pairs with D on task 2
* After a set time, switch the most experienced member of the pair out, leaving the least experienced member
* B pairs with C on task 1
* D pairs with A on task 2
* After a set time, switch to the person you haven't worked with yet
* D pairs with B on task 1
* A pairs with C on task 2

This can scale with more people. The idea is that everybody works with everybody else!
It is designed to encourage team task ownership and knowledge transfer.

### When to switch
Really only works on time, because both pairs need to switch at the same time. There needs to be enough time for a someone new to a task/returning to a task to get up to speed with what a task is/what has happened since they last worked on it. 90 mins is stated in the paper.

### Fun variations
Rather than switching to ensure everyone works with everyone else, just switch out the most experienced member of the pair each time. This ensures that less experienced developers get enough time to focus on a task, and learn a new skill/area of the codebase, whilst giving up some of the knowledge transfer benefits.


## Mob Programming

### Background
It's like Driver - Navigator, except there are multiple _navigators_.

### How to do it
* One person is the _driver_
* _navigators_ just shout out directions to follow
* _navigators_ have to debate between themselves if there is disagreement on direction

It can break deadlocks in a traditional Driver - Navigator setup, where the pair can't choose between different options of how to implement a particular feature.